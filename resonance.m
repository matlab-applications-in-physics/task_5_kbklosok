%MATLAB 2020b
%name: resonance.m
%author: kbklosok
%date: 9.12.2020
%version: 1.0
clear; clc;

%Constants

%GRAVITIONAL_ACCELERATION source:
%https://www.wolframalpha.com/widgets/gallery/view.jsp?id=d34e8683df527e3555153d979bcda9cf
%Location: Gliwice, Poland
GRAVITIONAL_ACCELERATION = 9.81059; %in m/s² 
STEEL_SHEAR_MODULUS = 79.3*10^9; %in Pa source: https://en.wikipedia.org/wiki/Shear_modulus
Coils = 40; %N
Gauge = 0.00211666667; %r in m
Mass = 0.100; %m in kg
TimeConstant = 10; %τ in s
DampingCoefficient = 1/(2*TimeConstant); %β in 1/s
DrivingForce = Mass*GRAVITIONAL_ACCELERATION; %F0 in N

%Variables
CoilRadius = 0.005:0.002:0.05; %R in m

SpringConstant = (STEEL_SHEAR_MODULUS*Gauge^4) ./ (4*Coils*CoilRadius.^3); %k in N/m
AngularFrequency = (SpringConstant./Mass).^(1/2); %ω0 = sqrt(k/m)

DrivingForceFrequency = 0:0.002:150; %in Hz / lower step = more accurate plots
DrivingForceAngularFrequency = 2*pi*DrivingForceFrequency; %ω = 2*pi*f

%Create empty array DrivingForceAngularFrequency  x CoilRadius
Amplitudes = zeros(length(DrivingForceFrequency),length(CoilRadius));

for Freq = 1:length(DrivingForceFrequency)
    Temp = AngularFrequency.^2 - DrivingForceAngularFrequency(Freq)^2;
    %Calculate amplitude every given frequency a for every coil radius
    Amplitudes(Freq, :) = (DrivingForce/Mass) ./ sqrt(Temp.^2  + 4*DampingCoefficient^2.*AngularFrequency.^2);
end


FileId = fopen('resonance_analysis_results.dat', 'w');
%Write used constants
Format = "%s;%f;%s\n";
fprintf(FileId, Format, "GRAVITIONAL_ACCELERATION:", GRAVITIONAL_ACCELERATION, "m/s²");
fprintf(FileId, Format, "STEEL_SHEAR_MODULUS:", STEEL_SHEAR_MODULUS,"Pa");
fprintf(FileId, Format, "NumberOfCoils:", Coils, "");
fprintf(FileId, Format, "CoilGauge:", Gauge, "m");
fprintf(FileId, Format, "Mass:", Mass, "kg");
fprintf(FileId, Format, "TimeConstant:", TimeConstant, "s");
fprintf(FileId, Format, "DrivingForce:", DrivingForce, "N");

%Write headlines
fprintf(FileId, "\n==========;==========;==========\n");
DataHeadlines = "\nCoilRadius[m];Amplitude[m];Frequency[m]\n";
fprintf(FileId, DataHeadlines);

%Get max amplitude for every coil radius and write data to file
MaxAmplitudes = zeros(1,length(CoilRadius));
for RadiusId = 1:length(CoilRadius)
    %Get max amplitude
    MaxAmplitudes(RadiusId) = max(Amplitudes(:,RadiusId));
    
    %Get frequency for max amplitude 
    MaxFrequencyId = find(Amplitudes(:,RadiusId)==max(Amplitudes(:,RadiusId)));
    
    %Write data to file
    fprintf(FileId, "%.3f; %.2f; %.3f\n", CoilRadius(RadiusId), MaxAmplitudes(RadiusId), DrivingForceFrequency(MaxFrequencyId));
end

%Create first plot
Fig = figure;
subplot(2,1,1);
plot(DrivingForceFrequency,Amplitudes(:,:));
xlabel('Driving Force Frequency [Hz]');
ylabel('Amplitude [m]');

%Create second plot
subplot(2,1,2);
plot(CoilRadius,MaxAmplitudes);
xlabel('Coil Radius [m]');
ylabel('Max Amplitude [m]');

%Save to file
saveas(Fig, 'resonance.pdf');

fclose('all');
